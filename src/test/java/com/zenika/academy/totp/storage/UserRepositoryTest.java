package com.zenika.academy.totp.storage;

import com.zenika.academy.totp.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryTest {
    
    private UserRepository repo;
    
    @BeforeEach
    void setUp() {
        repo = new UserRepository();
    }
    
    @Test
    @DisplayName("Renvoie vide pour un utilisateur quelconque si le repo est vide")
    void emptyRepo() {
        assertEquals(Optional.empty(), repo.findByEmail("academy@zenika.com"));
    }
    
    @Test
    @DisplayName("Retrouve l'utilisateur par son email")
    void saveAndFind() {
        repo.save(new User("a@b.c", "secret"));
        
        Optional<User> result = repo.findByEmail("a@b.c");
        assertTrue(result.isPresent());
        assertEquals("a@b.c", result.get().getEmail());
        assertEquals("secret", result.get().getSecretKey());
    }
}