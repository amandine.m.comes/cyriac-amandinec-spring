package com.zenika.academy.totp.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class SecretKeyGeneratorTest {
    @Test
    @DisplayName("Génère une chaine à partir d'un long aléatoire")
    void generate() {
        SecretKeyGenerator secretKeyGenerator = new SecretKeyGenerator(new Random(0));
        
        String actual = secretKeyGenerator.generate();
        
        assertEquals("4962768465676381896", actual);
    }
    
    @Test
    @DisplayName("Génère une chaine avec seulement des chiffres")
    void generatePositive() {
        // avec un stub
        SecretKeyGenerator secretKeyGenerator = new SecretKeyGenerator(new FakeRandom());
        
        String actual = secretKeyGenerator.generate();
        
        assertEquals("1655", actual);
    }

    private class FakeRandom extends Random {
        @Override
        public long nextLong() {
            return -1655;
        }
    }
}