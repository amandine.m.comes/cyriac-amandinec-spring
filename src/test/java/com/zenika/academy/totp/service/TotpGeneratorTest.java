package com.zenika.academy.totp.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.function.BiFunction;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TotpGeneratorTest {

    private static final Instant FAKE_NOW = Instant.ofEpochSecond(1123456789);
    private BiFunction<String, String, String> hashFunctionMock = Mockito.mock(BiFunction.class);
    private TotpGenerator totpGenerator = new TotpGenerator(Clock.fixed(FAKE_NOW, ZoneOffset.UTC), hashFunctionMock, 30);
    
    @BeforeEach
    void setup() {
        Mockito.reset(hashFunctionMock);    
    }
    
    @Test
    void totpGeneration() {
        Mockito.when(hashFunctionMock.apply("secret", "37448559")).thenReturn("fakeCode");
        
        String actual = totpGenerator.generate("secret");
        
        assertEquals("fakeCode", actual);
    }

}