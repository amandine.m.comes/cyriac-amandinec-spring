package com.zenika.academy.totp.service;

import java.util.Random;

/**
 * Service de génération d'une clé secrète.
 * 
 * La clé secrète pour notre exemple est un nombre entier (long). Dans la vraie vie, ça n'est pas assez d'octets pour être sécurisé !
 */
public class SecretKeyGenerator {
    
    private final Random random;

    public SecretKeyGenerator(Random random) {
        this.random = random;
    }

    public String generate() {
        return String.valueOf(Math.abs(random.nextLong()));
    }
}
