package com.zenika.academy.totp.service;

import com.zenika.academy.totp.storage.UserRepository;

public class LoginService {
    private final UserRepository userRepository;
    private final TotpGenerator totpGenerator;

    public LoginService(UserRepository userRepository, TotpGenerator totpGenerator) {
        this.userRepository = userRepository;
        this.totpGenerator = totpGenerator;
    }

    /**
     * Vérifie qu'un code est correct pour l'email donné.
     * @param userEmail l'email à utiliser pour le login
     * @param givenCode le code entré par l'utilisateur
     *                  
     * @return vrai si le code est correct, faux si le code est incorrect ou que l'utilisateur n'existe pas
     */
    public boolean attemptLogin(String userEmail, String givenCode) {
        boolean loginSuccess = userRepository.findByEmail(userEmail)
                .map(user -> totpGenerator.generate(user.getSecretKey()))
                .map(correctCode -> correctCode.equals(givenCode))
                .orElse(false);
        
        return loginSuccess;
    }
}
