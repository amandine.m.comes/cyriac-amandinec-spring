package com.zenika.academy.totp.model;

/**
 * Représente un utilisateur du système d'authentification
 */
public class User {

    /**
     * L'email est à la fois le llgin de l'utilisateur (il s'en sert pour s'authentifier) et son identifiant (il ne peut y avoir 
     * qu'un utilisateur pour un email donné)
     */
    private String email;

    /**
     * La clé secrète de l'utilisateur, utilisée pour l'algo de génération du TOTP
     */
    private String secretKey;

    public User(String email, String secretKey) {
        this.email = email;
        this.secretKey = secretKey;
    }

    public String getEmail() {
        return email;
    }

    public String getSecretKey() {
        return secretKey;
    }
}
