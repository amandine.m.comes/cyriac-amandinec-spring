package com.zenika.academy.totp.storage;

import com.zenika.academy.totp.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class UserRepository {
    
    private final Map<String, User> data = new HashMap<>();
    
    public void save(User newUser) {
        data.put(newUser.getEmail(), newUser);
    }
    
    public Optional<User> findByEmail(String email) {
        return Optional.ofNullable(data.get(email));
    }
}
