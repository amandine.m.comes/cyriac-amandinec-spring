package com.zenika.academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

    @SpringBootApplication
    public class CyriacAmandineSpringApplication {
        public static void main(String[] args) {
            SpringApplication.run(CyriacAmandineSpringApplication.class, args);
        }
    }

